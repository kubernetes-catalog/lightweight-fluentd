# Lightweight Fluentd Kubernetes Catalog

This repo is intended to be used as a Terraform module, to easily install Fluentd in your Kubernetes cluster.

> This repo is not intended to be used in production, 
> but rather as a really quick way to validate something which needs Fluentd

It uses primarily Terraform and the [Kustomization Terraform provider](https://registry.terraform.io/providers/kbst/kustomization/latest/docs),
to create visibility during changes, as well as pruning anything that is no longer needed.

The manifests are based off the default manifests provided by the Fluentd helm chart [here](https://github.com/fluent/helm-charts/tree/main/charts/fluentd)

This repo serves to wrap those manifests in the Kustomization provider.

## Usage

If you would like to use this module to install Fluentd, 
it can be done in the form of a Terraform module.

```hcl-terraform
module "fluentd" {
  source = "git::https://gitlab.com/kubernetes-catalog/lightweight-fluentd.git"
}
```

Once you had added the module, remember to run `terraform init`, for the module to be pulled.

### Using specific versions

The versions of this repo match the versions of the upstream [Fluentd helm chart](https://github.com/fluent/helm-charts/releases), prefixed by fluentd.

In order to use a specific version of the module, add the specific tag on the url.
 
```hcl-terraform
module "fluentd" {
  source = "git::https://gitlab.com/kubernetes-catalog/lightweight-fluentd.git?ref=0.2.4"
}
 ```
 
### Variables

The module also allows you to override certain values, which might be helpful to your installation.

They are listed in the example below 

```hcl-terraform
module "fluentd" {
  source = "git::https://gitlab.com/kubernetes-catalog/lightweight-fluentd.git?ref=0.2.4"
  
  # Override namespace for Fluentd components
  namespace = "fluentd"
  
  # Override Elasticsearch connection details
  elasticsearch-host = "elasticsearch-master.svc"
  elasticsearch-port = "9200"
}
```

## Developing

Developing the module locally is a fairly straight forward process, and works very much like any other Terraform development.

### Variables

Create a local variables file relevant to your environment. 

```bash
touch terraform.auto.tfvars
```

Check out the `input.tf` file for all of the available variables you could change, and add these to your variables file

```bash
echo 'kubeconfig="~/.kube/config" >> terraform.auto.tfvars'
```

### Installing the providers

```bash
terraform init
```

### Installing

You can now install Fluentd in your cluster.

```bash
terraform apply
```

The output will show you any changes which will occur in your cluster when the apply is run.

## Contributing

Contributions are always welcome. 

Feel free to open PRs / issues with ideas / changes. 

Changes to the manifests themselves will likely not be accepted, 
as these come directly from the upstream provider, 
and any changes will either create churn or a maintenance burden.

If you would like to contribute changes to any other part of the code base, 
feel free to open a PR, or suggest changes by opening an issue.