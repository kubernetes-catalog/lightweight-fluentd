data "kustomization_overlay" "fluentd-prerequisites" {
  resources = [
    "${path.module}/pre-install-manifests",
  ]

  namespace = var.namespace
}

data "kustomization_overlay" "fluentd" {
  resources = [
    "${path.module}/manifests",
  ]

  namespace = var.namespace

  # We need to make sure that the ClusterRoleBinding namespace matches the namespace where we are deploying,
  # as Kustomize doesn't override this.
  # If the namespace is different from the default we provide, the deployment will fail
  patches {
    patch = <<-EOF
      - op: replace
        path: /subjects/0/namespace
        value: ${var.namespace}
    EOF
    target = {
      kind = "ClusterRoleBinding"
      name = "fluentd"
    }
  }


  patches {
    patch = <<-EOF
      - op: add
        path: /spec/template/spec/containers/0/env/-
        value:
          name: FLUENT_ELASTICSEARCH_HOST
          value: ${var.elasticsearch-host}
    EOF
    target = {
      kind = "DaemonSet"
      name = "fluentd"
    }
  }

  patches {
    patch = <<-EOF
      - op: add
        path: /spec/template/spec/containers/0/env/-
        value:
          name: FLUENT_ELASTICSEARCH_PORT
          value: "${var.elasticsearch-port}"
    EOF
    target = {
      kind = "DaemonSet"
      name = "fluentd"
    }
  }

  patches {
    patch = <<-EOF
      - op: replace
        path: /data/04_outputs.conf
        value: |-
          <label @OUTPUT>
            <match **>
              @type elasticsearch
              host "${var.elasticsearch-host}"
              port 9200
              path ""
              user elastic
              password changeme
            </match>
          </label>
    EOF
    target = {
      kind = "ConfigMap"
      name = "fluentd-config"
    }
  }
}

/**
 * When the Kustomization provider run through the manifests,
 * it cannot ensure that the namespace will be created first,
 * due to a ID limitation in the Terraform provider SDK.
 *
 * We therefor have a directory with prerequisites,
 * which we can install first,
 * and use that in a depends_on,
 * to ensure all prerequisites are met for our manifests.
 *
 * We also use a directory with a kustomization built in,
 * to ensure we can use the manifests without necessarily needing Terraform,
 * and could still use Kustomize directly on them.
 */
resource "kustomization_resource" "fluentd-prerequisites" {
  for_each = data.kustomization_overlay.fluentd-prerequisites.ids

  manifest = data.kustomization_overlay.fluentd-prerequisites.manifests[each.value]
}

/**
 * Once all of the prerequisites exist,
 * we can continue to create all of the other manifests,
 * and let Kubernetes take care of the rest for us.
 */
resource "kustomization_resource" "fluentd" {
  depends_on = [kustomization_resource.fluentd-prerequisites]

  for_each = data.kustomization_overlay.fluentd.ids

  manifest = data.kustomization_overlay.fluentd.manifests[each.value]
}
