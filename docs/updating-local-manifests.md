# Updating manifests from latest release

```bash
rm -rf manifests/* fluentd/
helm repo add fluent https://fluent.github.io/helm-charts
helm repo update
helm fetch fluent/fluentd --untar
helm template fluentd \
    --output-dir manifests \
    --namespace fluentd \
    fluentd/
cp -r manifests/fluentd/templates/files.conf/* manifests/fluentd/templates/
rm -rf manifests/fluentd/templates/files.conf manifests/fluentd/templates/tests
cp -r manifests/fluentd/templates/* manifests/
pushd manifests/fluentd/templates/
for n in *; do printf '%s\n' "- $n"; done > ../../tmp-resources
cd ../../
cat <<EOF > kustomization.yml
resources:
$(cat tmp-resources)
EOF
popd
rm -rf manifests/fluentd manifests/tmp-resources fluentd
```